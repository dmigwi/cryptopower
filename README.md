# cryptopower

A cross-platform desktop wallet built with [gio](https://gioui.org/).

## Building

Note: You need to have [Go 1.19](https://golang.org/dl/) or above to build.

Then `go build`.

### Linux

To build **cryptopower** on Linux these [gio dependencies](https://gioui.org/doc/install/linux) are required.

Arch Linux:
`pacman -S vulkan-headers libxkbcommon-x11`

## FreeBSD

To build **cryptopower** on FreeBSD you will need to `pkg install vulkan-headers` as root. This is a gio dependency.

## Running cryptopower

### General usage

By default, **cryptopower** runs on Mainnet network type. However, cryptopower can run on testnet by issuing commands on the terminal in the format:

```bash
cryptopower [options]
```

- Run `./cryptopower --network=testnet` to run cryptopower on the testnet network.
- Run `cryptopower -h` or `cryptopower help` to get general information of commands and options that can be issued on the cli.
- Use `cryptopower <command> -h` or `cryptopower help <command>` to get detailed information about a command.

## Profiling

Cryptopower uses [pprof](https://github.com/google/pprof) for profiling. It creates a web server which you can use to save your profiles. To setup a profiling web server, run cryptopower with the --profile flag and pass a server port to it as an argument.

So, after running the build command above, run the command

`./cryptopower --profile=6060`

You should now have a local web server running on 127.0.0.1:6060.

To save a profile, you can simply use

`curl -O localhost:6060/debug/pprof/profile`

## Contributing

See [CONTRIBUTING.md](https://gitlab.com/cryptopower/cryptopower/blob/master/.gitlab/ci/CONTRIBUTING.md)

## Other

Earlier experimental work with other user interface toolkits can be found at [godcr-old](https://github.com/raedahgroup/godcr-old).

## Private Repo Notes

In order to use this repo you will need to configure git to use ssh instead of https:

create ~/.gitconfig:
```
[user]
    name = Nane
    email = some@email.address
[url "git@code.cryptopower.dev:"]
	insteadOf = https://code.cryptopower.dev/
```

create ~/.netrc:
```
machine code.cryptopower.dev
login <current shared auth username>
password <current shared auth password>
```
For `go get` commands to work you will need to set the `GOPRIVATE` variable. 
Example:
`export GOPRIVATE="code.cryptopower.dev/group/"`
